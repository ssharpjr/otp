One Time Pad (Pass) Generator

This series of scripts will generate One Time Pads as needed.

The original article can be found at: (https://www.americanpartisan.org/2020/01/r-pi-otp-dryad-true-hardware-rng-how-to/).

Tested on a RPI 4B running RaspbianOS Bullseye (2021-10-30-armhf).

You will need `rng-tools` installed to access the hardware RNG generator (optional, but recommended).

`sudo apt install rng-tools`


__otp.sh__ will need to run with sudo to access the RAMDisk function (`sudo ./otp.sh`)
