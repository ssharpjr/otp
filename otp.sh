#!/bin/bash
#
# One Time Pad Table Generator Programs
#
# This determines how many characters per group in the OTPs
blocksize=5
# This determines how many groups per line of the OTPs
blockrow=5
# This determines how many OTPs across the page to print
read -p "How many pads across the paper? " tablerow
# This determines how many lines of characters in each OTP
rowcount=10
# This determines how many rows of OTPs to print down the page
read -p "How many rows of pads? " pagecount
#numbers or letters in the OTP?
read -p "OTP of numbers 0-9 (n) or letters A-Z (l)? default = n " NorL
if [ "$NorL" == "l" ]
then
  TYPE="A-Z"
else
  TYPE="0-9"
read -p "Arabic numerals (a) or symbols [O\|/+A>V<X] (s)?" AorS
fi
# Have user pick algorithm to use for picking numbers or letters
read -p "Which RNG Device? random-(1), urandom-(2) or hwrng-(3). Default is 1 " DEVICE
case $DEVICE in
   3)
      RNGAlg=hwrng
      ;;
   2)
      RNGAlg=urandom
      ;;
   *)
      RNGAlg=random
      ;;
esac
# where to generate the page of OTPs – putting it in a ramdisk
otpath="/ramdisk/otp.txt"
# Create a ramdisk to put the tables in to keep them in memory and not write to SD card
mkfs -q /dev/ram1 1024
mkdir -p /ramdisk
mount /dev/ram1 /ramdisk
echo
echo "$RNGAlg it is…"
echo
echo "Generating a Table of $tablerow by $pagecount of ($TYPE – type) OTPs."
echo
echo "                 grinding away…"
echo
# Reset BASH time counter
SECONDS=0
# Generate a row of OTPs
for ((x=1; x<=$pagecount; x++))
do
# Generate a full line of characters for eact OTP in the current row of OTPs
#  echo "" >> $otpath;
    for ((i=1; i<=$rowcount; i++))
    do
        # Generate a row of groups for the current OTP in the current row of OTPs
        for ((k=1; k<=$tablerow; k++))
        do
            # Generate the groups for the current line of groups in the current OTP
            for ((j=1; j<=$blockrow; j++))
            do
                # Generate the current group of characters using the selected atributes
                #    NOTE: using this if-then-else here instead of earlier in the script with two
                #    longer generating branches results in an extra 2-seconds per OTP to generate with
                #    hwrng, while reducing script length
                if [ "$NorL" == "l" ]
                then
                    randnum=$(base32 /dev/$RNGAlg | tr -dc $TYPE | head -c $blocksize)
                else
                    randnum=$(xxd -p /dev/$RNGAlg | tr -dc [:digit:] | head -c $blocksize)
                    if [ "$AorS" == "s" ]
                    then
                        randnum="$(echo $randnum | tr '0123456789' 'O\\|/+A>V<X')"
                        # the first \ is required to make the second \ display – it is otherwise a special meaning character
                    fi
                fi
                echo -n $randnum >> $otpath
                echo -n " " >> $otpath
            done
            echo -n "   " >> $otpath
        done
      echo "" >> $otpath
    done
    echo "" >> $otpath
    echo "" >> $otpath
done
# Send the table to X’s clipboard
xclip -i /ramdisk/otp.txt -sel clip
echo
echo "The OTP table is now in X’s clipboard."
echo
ELAPSED="$(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"
echo "Time taken to generate the OTP Table was $ELAPSED"
echo
# Prepare to erase the table from the ramdisk
read -p "Press (return) to delete the OTP table from the ramdisk…" dumpit
rm $otpath
echo
echo "Don’t forget to clear X’s clipboard when you are done!!!"